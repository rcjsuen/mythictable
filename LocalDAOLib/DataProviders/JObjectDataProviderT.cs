﻿using LocalDAOLib.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace DataAccessLib.DataProviders
{
    public class JObjectDataProvider<T> : IDataProvider<T> where T: IIndexable
    {
        private IDataAccessObject _dataAccessObject;

        public JObjectDataProvider(IDataAccessObject dataAccessObject)
        {
            _dataAccessObject = dataAccessObject;
        }

        public IEnumerable<T> GetAll()
        {
            IEnumerable<JObject> jArray = (IEnumerable<JObject>)_dataAccessObject.Get<T>((x) => true);
            List<T> itemList = new List<T>();
            foreach (JObject item in jArray)
            {
                itemList.Add(item.ToObject<T>());
            }
            return itemList;
        }

        public T GetByID(int id)
        {
            JObject jItem = (JObject)_dataAccessObject.Get<T>((x) => x.id == id).First();
            return jItem.ToObject<T>();
        }

        public IEnumerable<T> Search(Func<T, bool> selector) 
        {
            IEnumerable<JObject> jArray = (IEnumerable<JObject>)_dataAccessObject.Get(selector);
            List<T> itemList = new List<T>();
            foreach (JObject item in jArray)
            {
                itemList.Add(item.ToObject<T>());
            }
            return itemList;
        }

        public void Store(T item)
        {
            JObject jItem = new JObject(item);

            _dataAccessObject.CreateOrUpdate(jItem);
        }

        public void Store(T[] items)
        {
            foreach (T item in items)
            {
                Store(item);
            }
        }
    }
}
