﻿namespace LocalDAOLib.Models
{
    public interface IIndexable
    {
        public int id { get; set; }
    }
}
