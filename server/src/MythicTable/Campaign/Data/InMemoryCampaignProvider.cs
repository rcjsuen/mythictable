using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MythicTable.Campaign.Exceptions;

namespace MythicTable.Campaign.Data
{
    public class InMemoryCampaignProvider : ICampaignProvider
    {
        private readonly Dictionary<string, CampaignDTO> campaigns;
        private readonly Dictionary<string, List<MessageDto>> messages = new Dictionary<string,List<MessageDto>>();

        public InMemoryCampaignProvider()
        {
            campaigns = new Dictionary<string,CampaignDTO>();
        }
        public Task<List<CampaignDTO>> GetAll(string userId)
        {
            Console.WriteLine(userId);
            return Task.FromResult(
                campaigns.Values.ToArray<CampaignDTO>().Where(campaign => {
                    return campaign.Owner == userId || campaign.Players.Any(player => player.Name == userId);
                }).ToList<CampaignDTO>()
            );
        }

        public Task<CampaignDTO> Get(string campaignId)
        {
            campaigns.TryGetValue(campaignId,out var campaign);
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Cannot find campaign of id {campaignId}");
            }

            return Task.FromResult(campaign);
        }

        public Task<CampaignDTO> GetByJoinId(string joinId)
        {
            try
            {
                return Task.FromResult(campaigns.Values.Single(c => c.JoinId == joinId));
            }
            catch (InvalidOperationException e)
            {
                throw new CampaignNotFoundException($"Cannot find campaign with join id {joinId}");
            }
        }

        public Task<CampaignDTO> Create(CampaignDTO campaign, string owner)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaign.Id != null && campaign.Id.Length != 0)
            {
                throw new CampaignInvalidException($"The Campaign already has an id");
            }

            campaign.Owner = owner;
            campaign.Id = Guid.NewGuid().ToString();
            this.campaigns[campaign.Id] = campaign;
            this.messages.Add(campaign.Id, new List<MessageDto>());
            return Task.FromResult(campaign);
        }

        public Task<CampaignDTO> Update(string campaignId, CampaignDTO campaign)
        {
            if (campaign == null)
            {
                throw new CampaignInvalidException($"The campaign is null");
            }

            if (campaignId == null || campaignId.Length == 0)
            {
                throw new CampaignInvalidException($"The Campaign MUST have an id");
            }

            campaign.Id = campaignId;
            campaigns[campaignId] = campaign;

            return Task.FromResult(campaign);
        }

        public Task Delete(string campaignId)
        {
            var campaign =  this.Get(campaignId);
            campaigns.Remove(campaignId);
            return Task.CompletedTask;
        }

        public async Task<List<PlayerDTO>> GetPlayers(string campaignId)
        {
            var campaign = await this.Get(campaignId);
            return campaign.Players;
        }
        
        public async Task<CampaignDTO> AddPlayer(string campaignId, PlayerDTO player)
        {
            var campaign = await this.Get(campaignId);
            
            if (campaign == null)
            {
                throw new CampaignNotFoundException($"Add Player. Cannot find campaign of id {campaignId}");
            }
            
            if (campaign.Players.Any(m => m.Name == player.Name))
            {
                throw new CampaignAddPlayerException($"The player '{player.Name}' is already in campaign {campaignId}");
            }

            campaign.Players.Add(new PlayerDTO
            {
                Name = player.Name
            });
            
            await this.Update(campaignId, campaign);
            return campaign;
        }

        public Task<CampaignDTO> RemovePlayer(string campaignId, PlayerDTO player)
        {
            var campaign =  this.Get(campaignId).Result;

            var numberRemoved = campaign.Players.RemoveAll(membership => membership.Name == player.Name);
            if (numberRemoved == 0)
            {
                throw new CampaignRemovePlayerException($"The player '{player.Name}' is not in campaign {campaignId}");
            }

            this.Update(campaignId, campaign);
            return Task.FromResult(campaign);
        }

        public Task<List<MessageDto>> GetMessages(string campaignId)
        {
            if(!this.messages.ContainsKey(campaignId))
            {
                return Task.FromResult(new List<MessageDto>());
            }
            return Task.FromResult(messages[campaignId]);
        }
        
        public Task<MessageDto> AddMessage(string campaignId, MessageDto message)
        {
            if(!this.messages.ContainsKey(campaignId))
            {
                this.messages.Add(campaignId, new List<MessageDto>());
            }
            var nextIndex = this.messages[campaignId].Count + 1;
            message.Id = nextIndex.ToString();
            this.messages[campaignId].Add(message);
            return Task.FromResult(message);
        }
    }
}
