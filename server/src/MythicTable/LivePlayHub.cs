﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using MythicTable.Campaign.Data;
using MythicTable.GameSession;
using MythicTable.Collections.Providers;
using Newtonsoft.Json.Linq;
using MythicTable.Collections.Data;
using MythicTable.Filters;
using MythicTable.Profile;
using MythicTable.Profile.Data;
using MythicTable.TextParsing;
using Newtonsoft.Json;

namespace MythicTable
{
    public class LivePlayHub : Hub<ILiveClient>
    {
        private ICampaignProvider CampaignProvider { get; }
        private ICollectionProvider CollectionProvider { get; }
        private readonly ILogger logger;

        private readonly ChatParser parser;

        private readonly ProfileCache cache;

        public LivePlayHub(
            ICampaignProvider campaignProvider,
            ICollectionProvider collectionProvider,
            IProfileProvider profileProvider,
            IMemoryCache memoryCache,
            ILogger<LivePlayHub> logger)
        {
            CampaignProvider = campaignProvider;
            CollectionProvider = collectionProvider;
            this.logger = logger;
            parser = new ChatParser(new SkizzerzRoller());
            cache = new ProfileCache(profileProvider, memoryCache);
        }

        private async Task<bool> IsCampaignMember(string campaignId)
        {
            // Check if user is an owner or player of the campaign
            var campaign = await this.CampaignProvider.Get(campaignId);
            var userId = await this.GetUserId();
            return (campaign.Owner == userId || campaign.Players.Exists(player => player.Name == userId));
        }

        [Authorize]
        public async Task<bool> JoinSession(string sessionId)
        {
            if (!(await IsCampaignMember(sessionId))) { return false; }

            this.logger.LogInformation($"Joining session {sessionId}");
            await Groups.AddToGroupAsync(Context.ConnectionId, sessionId);
            return true;
        }

        [Authorize]
        public async Task<bool> LeaveSession(string sessionId)
        {
            if (!(await IsCampaignMember(sessionId))) { return false; }

            await Groups.RemoveFromGroupAsync(Context.ConnectionId, sessionId);
            return true;
        }

        [Authorize]
        public async Task<bool> SendMessage(string sessionId, MessageDto message)
        {
            if (!(await IsCampaignMember(sessionId))) { return false; }

            var results = parser.Process(message.Message);
            message.Result = results.AsDto();
            this.logger.LogInformation($"Dice Roll - User: {message.UserId} Roll: {message.Message} Results: {message.Result.Dice} Message: {message.Result.Message}");
            var campaignId = message.SessionId;
            await CampaignProvider.AddMessage(campaignId, message);
            await Clients.Group(sessionId).SendMessage(message);
            return true;
        }

        // TODO - Delete this
        [Authorize]
        [HubMethodName("submitDeltaTemp")]
        public async Task<bool> RebroadcastDeltaTemp(string sessionId, SessionOpDelta delta)
        {
            await Clients.Group(sessionId).ConfirmOpDelta(delta);
            return true;
        }

        [Authorize]
        public async Task<bool> DrawLine(string sessionId, JObject lineData)
        {
            if (!(await IsCampaignMember(sessionId))) { return false; }

            await Clients.Group(sessionId).DrawLine(lineData);
            return true;
        }

        [Authorize]
        public async Task<JObject> AddCollectionItem(string sessionId, string collection, string campaignId, JObject item)
        {
            if (!(await IsCampaignMember(sessionId))) { return null; }

            var obj = await CollectionProvider.CreateByCampaign(await this.GetUserId(), collection, campaignId, item);
            await Clients.Group(sessionId).ObjectAdded(collection, obj);
            return obj;
        }

        [Authorize]
        public async Task<JObject> UpdateObject(string sessionId, UpdateCollectionHubParameters parameters)
        {
            try
            {
                if (!(await IsCampaignMember(sessionId))) { return null; }

                if (await CollectionProvider.UpdateByCampaign(parameters.Collection, parameters.CampaignId, parameters.Id, parameters.Patch) > 0)
                {
                    await Clients.Group(sessionId).ObjectUpdated(parameters);
                    return await CollectionProvider.GetByCampaign(parameters.Collection, parameters.CampaignId, parameters.Id);
                }
            }
            catch (Exception)
            {

                this.logger.LogError($"Error encountered in UpdateObject({sessionId}, ...)");
                this.logger.LogError($"parameters={JsonConvert.SerializeObject(parameters)}");
                throw;
            }
            return null;
        }

        [Authorize]
        public async Task<bool> RemoveObject(string sessionId, string collection, string id)
        {
            if (!(await IsCampaignMember(sessionId))) { return false; }

            if (await CollectionProvider.Delete(await this.GetUserId(), collection, id) > 0)
            {
                await Clients.Group(sessionId).ObjectRemoved(collection, id);
                return true;
            }
            return false;
        }

        [Authorize]
        public async Task<bool> RemoveCampaignObject(string sessionId, string collection, string id)
        {
            if (!(await IsCampaignMember(sessionId))) { return false; }

            if (await CollectionProvider.DeleteByCampaign(collection, sessionId, id) > 0)
            {
                await Clients.Group(sessionId).ObjectRemoved(collection, id);
                return true;
            }
            return false;
        }

        private Task<string> GetUserId()
        {
            var userId = this.Context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return cache.CacheTryGetValueSet(userId);
        }
    }
}
