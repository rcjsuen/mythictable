using System.Threading.Tasks;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Exceptions;
using MythicTable.TestUtils.Util;
using Xunit;

namespace MythicTable.Tests.Campaign
{
    public class InMemoryCampaignProviderTests : IAsyncLifetime
    {
        private ICampaignProvider provider;
        private const string DoesntExistId = "551137c2f9e1fac808a5f572";
        private const string CampaignOwner = "test player";
        public Task InitializeAsync()
        {
            provider = new InMemoryCampaignProvider();
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async void TestCreateCampaignAsync()
        {
            var campaign = new CampaignDTO{Name = "Test"};

            var resultCampaign = await provider.Create(campaign, CampaignOwner);
            Assert.Equal(campaign.Name, resultCampaign.Name);
            Assert.Equal(CampaignOwner, resultCampaign.Owner);
        }

        [Fact]
        public async void TestNullCampaignThrowsExceptionOnCreateAsync()
        {
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Create(null, CampaignOwner));
        }

        [Fact]
        public async void TestCampaignWithIdThrowsExceptionOnCreateAsync()
        {
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Create(new CampaignDTO{Id = DoesntExistId}, CampaignOwner));
        }

        [Fact]
        public async void TestUpdateAsync()
        {
            var testCampaign = await CreateCampaign();
            testCampaign.Name = "Modified";

            var resultingCampaign = await provider.Update(testCampaign.Id, testCampaign);
            
            Assert.Equal(testCampaign.Name, resultingCampaign.Name);
        }

        [Fact]
        public async void TestNullCampaignThrowsExceptionOnUpdateAsync()
        {
            var testCampaign = await CreateCampaign();
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Update(testCampaign.Id, null));
        }

        [Fact]
        public async void TestCampaignWithoutIdThrowsExceptionOnUpdateAsync()
        {
            await Assert.ThrowsAsync<CampaignInvalidException>(() => provider.Update(null, new CampaignDTO{Name = "test"}));
        }

        [Fact]
        public async void TestGetCampaignAsync()
        {
            var testCampaign = await CreateCampaign();

            var results = await provider.Get(testCampaign.Id);
            Assert.Equal(testCampaign.Name, results.Name);
            Assert.Equal(testCampaign.Id, results.Id);
        }

        [Fact]
        public async void TestGetReturnsNoCampaignsAsync()
        {
            await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.Get(DoesntExistId));
        }

        [Fact]
        public async void TestGetAllCampaignsAsync()
        {
            var results1 = await provider.GetAll(CampaignOwner);
            var testCampaign1 = await CreateCampaign();
            var testCampaign2 = await CreateCampaign();
            await CreateCampaign("test owner");
            var results2 = await provider.GetAll(CampaignOwner);

            Assert.Equal(2, results2.Count - results1.Count);
            Assert.Equal(testCampaign1.Id, results2[^2].Id);
            Assert.Equal(testCampaign1.Name, results2[^2].Name);
            Assert.Equal(testCampaign2.Id, results2[^1].Id);
            Assert.Equal(testCampaign2.Name, results2[^1].Name);
        }

        [Fact]
        public async void TestGetAllCampaignsForPlayersAsync()
        {
            var campaign = await CreateCampaign();
            await provider.AddPlayer(campaign.Id, new PlayerDTO { Name = "other player" });
            var results = await provider.GetAll("other player");

            Assert.Equal(1, results.Count);
        }

        [Fact]
        public async void TestDeleteAsync()
        {
            var testCampaign = await CreateCampaign();
            await provider.Delete(testCampaign.Id);
            await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.Get(testCampaign.Id));
        }

        [Fact]
        public async void TestDeleteInvalidIdFailsAsync()
        {
            await Assert.ThrowsAsync<CampaignNotFoundException>(() => provider.Delete(DoesntExistId));
        }

        [Fact]
        public async void TestPlayersAreEmptyWithNewCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var result = await provider.GetPlayers(testCampaign.Id);
            Assert.Empty(result);
        }

        [Fact]
        public async void TestAddPlayersToCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var campaign = await provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = CampaignOwner});
            Assert.Single(campaign.Players);
        }

        [Fact]
        public async void TestAddPlayerTwiceCausesErrorAsync()
        {
            var testCampaign = await CreateCampaign();
            await provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = CampaignOwner});
            await Assert.ThrowsAsync<CampaignAddPlayerException>(() => provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = CampaignOwner}));
        }

        [Fact]
        public async void TestRemovingPlayerFromEmptyCampaignCausesErrorAsync()
        {
            var testCampaign = await CreateCampaign();
            await Assert.ThrowsAsync<CampaignRemovePlayerException>(() => provider.RemovePlayer(testCampaign.Id, new PlayerDTO{Name = CampaignOwner}));
        }

        [Fact]
        public async void TestRemovingPlayerRemovesThemFromCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var player = new PlayerDTO{Name = CampaignOwner};
            await provider.AddPlayer(testCampaign.Id, player);
            await provider.RemovePlayer(testCampaign.Id, player);
            await Assert.ThrowsAsync<CampaignRemovePlayerException>(() => provider.RemovePlayer(testCampaign.Id, player));
        }

        [Fact]
        public async void TestRemovingWrongPlayerCausesErrorAsync()
        {
            var testCampaign = await CreateCampaign();
            var response = await provider.AddPlayer(testCampaign.Id, new PlayerDTO{Name = CampaignOwner});
            await Assert.ThrowsAsync<CampaignRemovePlayerException>(() => provider.RemovePlayer(testCampaign.Id, new PlayerDTO{Name = "WRONG player"}));
        }

        [Fact]
        public async void TestPlayersShowInCampaignAsync()
        {
            var testCampaign = await CreateCampaign();
            var player = new PlayerDTO{Name = CampaignOwner};
            await provider.AddPlayer(testCampaign.Id, player);

            var results = await provider.Get(testCampaign.Id);
            
            Assert.Single(results.Players);
        }

        [Fact]
        public async void TestGetMessageFromNonExistentCampaignReturnsEmptyListAsync()
        {
            var results = await provider.GetMessages(DoesntExistId);
            Assert.Empty(results);
        }

        [Fact]
        public async void TestAddRollAsync()
        {
            var testCampaign = await CreateCampaign();

            var roll = new MessageDto();
            await provider.AddMessage(testCampaign.Id, roll);
            var results = await provider.GetMessages(testCampaign.Id);
            
            Assert.Single(results);
        }

        [Fact]
        public async void TestAddRollToNonExistentCampaignJustWorksAsync()
        {
            var roll = new MessageDto();
            await provider.AddMessage(DoesntExistId, roll);
            var results = await provider.GetMessages(DoesntExistId);

            Assert.Single(results);
        }

        private async Task<CampaignDTO> CreateCampaign(string owner=CampaignOwner)
        {
            return await provider.Create(new CampaignDTO{Name = StringHelpers.RandomString(8)}, owner);
        }
    }
}