using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Controller;
using MythicTable.Profile.Data;
using MythicTable.Collections.Providers;
using MythicTable.Common.Exceptions;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json.Linq;
using Xunit;


namespace MythicTable.Tests.Collections.Controller
{
    public class CollectionsControllerTest : IAsyncLifetime
    {
        private const string COLLECTION_TYPE = "TestCollection";
        private const string DOESNT_EXIST_ID = "551137c2f9e1fac808a5f572";

        private CollectionsController controller;
        private ICampaignProvider campaignProvider;

        private ICollectionProvider collectionProvider;
        private Mock<IProfileProvider> profileProvider;

        private string User { get; set; } = "Jon";
        private string OtherUser { get; set; } = "Karen";
        private ProfileDto Profile { get; set; }

        public Task InitializeAsync()
        {
            campaignProvider = new InMemoryCampaignProvider();
            collectionProvider = new InMemoryCollectionProvider(Mock.Of<ILogger<InMemoryCollectionProvider>>());
            profileProvider = new Mock<IProfileProvider>();
            Profile = ProfileTestUtil.CreateProfile(profileProvider, User);
            controller = new CollectionsController(collectionProvider, campaignProvider, profileProvider.Object, new MemoryCache(new MemoryCacheOptions()));

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(hc => hc.User.FindFirst(It.IsAny<string>()))
                           .Returns(() => new Claim("", User));
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async Task Delete()
        {
            var obj = await controller.Post(COLLECTION_TYPE, new JObject
            {
                { "foo", "bar" }
            });

            var deleted = await controller.Delete(COLLECTION_TYPE, obj.GetId());
            Assert.Equal(1, deleted["numberDeleted"]);
        }

        [Fact]
        public async Task DeleteByCampaign()
        {
            //Arrange
            var campaign = campaignProvider.Create(new CampaignDTO(), controller.GetUserId());
            var player = campaignProvider.AddPlayer(campaign.Id.ToString(), new PlayerDTO());

            //Act
            JObject deleted =
                await controller.DeleteByCampaign(COLLECTION_TYPE, campaign.Id.ToString(), player.Id.ToString());
            bool playerExistsAfterDeletion = (int)deleted["numberDeleted"] > 0;

            //Assert
            Assert.False(playerExistsAfterDeletion);
        }
    }
}