<!--- TITLE: Incident report: YYYY-MM-DD - Incident Name -->
## Description
_Brief description of the problem, try to focus on the business impact_

## Timeline
* Time to detect: Xd Xh Xm (Time between start-of-problem and detection of the problem)
* Time to resolve: Xd Xh Xm (Time between start-of-problem and resolution of the problem)
* Detection method: XXXXX (How it was discovered)
* Human awareness method: XXXXXX (How a person became aware, page, user report, testing, etc) 

All Timestamps in PST
+ 2019-08-16T10:09:36 || Start of incident
+ 2019-08-16T10:09:36 || Thing that happened
+ 2019-08-16T10:09:36 || Incident detection
+ 2019-08-16T10:09:36 || Thing that happened
+ 2019-08-16T10:09:36 || Incident resolution

## Contributing Factor(s)
_Technical explanation of the issue. Should define the contributing factor(s) and why it's an issue._
* The technical factor(s) that led to this issue happening.

## Stabilization Steps
_What specific steps and actions were taken to stabilize the issue. This does not always entail a "fix" as further actions should be listed under "corrective actions"_
* Step(s) taken to fix/mitigate the issue.

## Impact
_What was the impact of the incident? How many requests/calls/customers were affected._
* Users couldn't X, Y, Z 

## Post Mortem Response Preamble
_This is a blameless Post Mortem._

_The goal is to understand how an accident could have happened, in order to better equip ourselves from it happening in the future # We will not focus on the past events as they pertain to "could've", "should've", etc._

_All follow-up action items will be assigned to a team/individual before the end of the meeting and tracked in GitLab. If the item is not going to be top priority leaving the meeting, don't make it a follow-up item._

_Participate. Be honest. Shields down, this is a safe space._


## Participants
_Those involved in the PMR. (Hint: use @)_

## Problems with our response
_What issues were uncovered with the incident with how we responded as a team. Slow responses, uncoordinated responses, poor communication, getting negative, getting too positive, etc._

## Ask Hows?
_Start with the immediate incident, ask "How did this happen?". List all the factors. For each ask "How" that happened. Each level of asking "How" should be moving from the pointy end of the organization (servers, code, power supplies, etc.) through to the blunt end of the organization (policies, procedures, project plans, budgets, contracts, etc.)_

_Asking “how?” gets you to describe (at least some) of the conditions that allowed an event to take place, and provides rich operational data._

## Improvement Measures
_Actionable items that can be done in the Near Term, that must have owners assigned to them. These owners are responsible for creating the tickets in Gitlab (at the minimum)._

## Considered and not committed to
_Potential improvement measures that were discussed but decided against because of time, feasibility, or potential lack of usefulness._
